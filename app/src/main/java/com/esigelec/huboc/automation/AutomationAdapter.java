package com.esigelec.huboc.automation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esigelec.huboc.ui.main.PlaceholderFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.esigelec.huboc.R;

public class AutomationAdapter extends RecyclerView.Adapter< AutomationAdapter.AutomationViewHolder> {

    Context context;

    ArrayList<AutomationDetails> listAutomationDetails;

    public AutomationAdapter(Context context, ArrayList<AutomationDetails> listAutomationDetails) {
        this.context = context;
        this.listAutomationDetails = listAutomationDetails;
    }

    @NonNull
    @Override
    public AutomationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_automation,parent,false);
        return new AutomationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AutomationViewHolder holder, int position) {

        AutomationDetails details = listAutomationDetails.get(position);
        holder.temperature.setText(details.getTemperature()+" °C");
        holder.co2Content.setText(details.getCo2Content()+" ppm");
        holder.humidity.setText(details.getHumidity()+" %");
        long timeSec=details.getTime();
        long millis=timeSec*1000;
        Date date = new Date(millis);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE,MMMM d,yyyy h:mm,a", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = sdf.format(date);
        //holder.time.setText(details.getTime()+"");
        holder.time.setText(formattedDate);
    }

    @Override
    public int getItemCount() {
        return listAutomationDetails.size();
    }

    public static class AutomationViewHolder extends RecyclerView.ViewHolder{

        TextView temperature;
        TextView co2Content;
        TextView time;
        TextView humidity;

        public AutomationViewHolder(@NonNull View itemView) {
            super(itemView);
            temperature=itemView.findViewById(R.id.temperatureAutomation);
            co2Content=itemView.findViewById(R.id.co2ContentAutomation);
            time=itemView.findViewById(R.id.timeAutomation);
            humidity=itemView.findViewById(R.id.humidityAutomation);
        }
    }
}
