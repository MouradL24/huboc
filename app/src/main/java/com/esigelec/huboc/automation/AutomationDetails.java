package com.esigelec.huboc.automation;

public class AutomationDetails {

    float temperature;
    float humidity;
    int co2Content;
    long time;

    public AutomationDetails() {

    }

    public AutomationDetails(float temperature, float humidity, int co2Content, long time) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.co2Content = co2Content;
        this.time = time;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public int getCo2Content() {
        return co2Content;
    }

    public void setCo2Content(int co2Content) {
        this.co2Content = co2Content;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }
}
