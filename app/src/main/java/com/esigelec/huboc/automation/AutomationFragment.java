package com.esigelec.huboc.automation;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esigelec.huboc.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;



import java.util.ArrayList;


public class AutomationFragment extends Fragment {

    RecyclerView recyclerView;
    DatabaseReference db;
    AutomationAdapter adapter;
    ArrayList<AutomationDetails> listAutomationDetails;
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.automation_fragment_layout, container, false);

        /*progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching Data...");
        progressDialog.show();*/

        db = FirebaseDatabase.getInstance("https://hubocbase-default-rtdb.europe-west1.firebasedatabase.app/").getReference("automation");
        recyclerView = view.findViewById(R.id.recycler_view_automation);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        listAutomationDetails = new ArrayList<AutomationDetails>();
        listAutomationDetails.add(new AutomationDetails(15,15,400,1515151517));
        adapter = new AutomationAdapter(view.getContext(), listAutomationDetails);
        recyclerView.setAdapter(adapter);

        EventChangeListener();
        return view;
    }

    private void EventChangeListener() {
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //Gson gson = new Gson();
                listAutomationDetails.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    String strTemperature = dataSnapshot.child("temperature").getValue(String.class);
                    String strHumidity = dataSnapshot.child("humidity").getValue(String.class);
                    String strCo2Content = dataSnapshot.child("co2Content").getValue(String.class);
                    String strTime = dataSnapshot.child("time").getValue(String.class);
                    float temperature=Float.parseFloat(strTemperature);
                    float humidity=Float.parseFloat(strHumidity);
                    int co2Content=Integer.parseInt(strCo2Content);
                    long time=Long.parseLong(strTime);
                    AutomationDetails details=new AutomationDetails(temperature,humidity,co2Content,time);
                    //AutomationDetails details = dataSnapshot.getValue(AutomationDetails.class);
                    listAutomationDetails.add(details);
                }
                adapter.notifyDataSetChanged();
                notification();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void notification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("n", "n", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getView().getContext().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getView().getContext(),"n")
                .setContentText("Data received !!!")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setContentText("Automation data received");

        NotificationManagerCompat managerCompat=NotificationManagerCompat.from(getView().getContext());
        managerCompat.notify(999,builder.build());


    }


}
