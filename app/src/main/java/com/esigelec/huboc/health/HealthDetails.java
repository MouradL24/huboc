package com.esigelec.huboc.health;

public class HealthDetails {

    int heartRate;
    int time;
    public static final int alertRate = 100;

    public HealthDetails(int heartRate, int time) {
        this.heartRate = heartRate;
        this.time = time;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}

