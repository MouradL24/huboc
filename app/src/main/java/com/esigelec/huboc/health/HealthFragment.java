package com.esigelec.huboc.health;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.esigelec.huboc.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HealthFragment extends Fragment {

    LineChart lineChart;
    LineDataSet lineDataSet;
    ArrayList<ILineDataSet>datasets;
    LineData lineData;
    ArrayList<HealthDetails>listHealthDetails;
    ArrayList<Entry>dataHealthDetails;
    DatabaseReference db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.health_fragment_layout,container,false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = FirebaseDatabase.getInstance("https://hubocbase-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("health1");

        listHealthDetails=new ArrayList<>();
        dataHealthDetails=new ArrayList<Entry>();

        lineChart=view.findViewById(R.id.linechart);
        lineChart.setDrawBorders(true);
        lineChart.setBorderColor(Color.BLUE);

        Description description=new Description();
        description.setText("Body Activity");
        description.setTextColor(Color.BLUE);
        description.setTextSize(20);
        lineChart.setDescription(description);

       /* XAxis xAxis=lineChart.getXAxis();
        YAxis yAxisLeft=lineChart.getAxisLeft();
        YAxis yAxisRight=lineChart.getAxisRight();
        xAxis.setValueFormatter(new MyAxisValueFormatter());*/

        updateDataHeartRate();
        //lineDataSet=new LineDataSet(dataHealthDetails,"contractions (beats) of the heart per minute (bpm)");
        lineDataSet=new LineDataSet(null,null);
        lineDataSet.setLineWidth(2);
        lineDataSet.setValueTextSize(10);
        /*lineDataSet.setColors(Color.GREEN);
        lineDataSet.setDrawCircles(true);
        lineDataSet.setDrawCircleHole(true);
        lineDataSet.setCircleColor(Color.BLUE);
        lineDataSet.setCircleHoleColor(Color.BLUE);
        lineDataSet.setCircleRadius(10);
        lineDataSet.setCircleHoleRadius(5);*/
        datasets=new ArrayList<>();
        EventChangeListener();
    }

    private ArrayList<Entry> dataValues(){
        ArrayList<Entry>dataValues=new ArrayList<Entry>();
        dataValues.add(new Entry(0,20));
        dataValues.add(new Entry(1,2));

        dataValues.add(new Entry(2,24));
        dataValues.add(new Entry(3,16));
        dataValues.add(new Entry(4,35));

        return dataValues;
    }

    private void updateDataHeartRate(){
        //dataHealthDetails.clear();
        for(HealthDetails details : listHealthDetails){
            dataHealthDetails.add(new Entry(details.getTime(),details.getHeartRate()));
        }
    }

    public void updateDataIncome() {
        Log.e("updateTag", "Updated Income");
        updateDataHeartRate();
        lineDataSet.setValues(dataHealthDetails);
        //lineDataSet.setLabel("beats of the heart per minute (bpm)");
        lineDataSet.setLabel("m/s");
        datasets.clear();
        datasets.add(lineDataSet);
        lineData=new LineData(datasets);
        lineChart.setData(lineData);
        lineChart.invalidate();
    }


    private class MyAxisValueFormatter extends ValueFormatter implements IAxisValueFormatter{

        @Override
        public String getFormattedValue(float value, AxisBase axis) {

            /*Replace null by "Day" + value
            * axis.setLabelCount(3,true)*/
            return null;

        }
    }

    private void EventChangeListener() {
        Query lastQuery = db.orderByKey().limitToLast(5);
        lastQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listHealthDetails.clear();
                if(snapshot.hasChildren()) {
                    int i=0;
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    String strData = dataSnapshot.child("data").getValue().toString();
                    String strTime = dataSnapshot.child("time").getValue().toString();

                    String strX1=strData.substring(0,2);
                    String strX2=strData.substring(2,4);
                    String strY1=strData.substring(4,6);
                    String strY2=strData.substring(6,8);
                    String strZ1=strData.substring(8,10);
                    String strZ2=strData.substring(10,12);
                    String strSignX=strData.substring(12,14);
                    String strSignY=strData.substring(14,16);
                    String strSignZ=strData.substring(16,18);
                    int X = Integer.parseInt(strX1,16)*100+Integer.parseInt(strX2,16);
                    int Y = Integer.parseInt(strY1,16)*100+Integer.parseInt(strY2,16);
                    int Z = Integer.parseInt(strZ1,16)*100+Integer.parseInt(strZ2,16);
                    int signX=Integer.parseInt(strSignX,16);
                    int signY=Integer.parseInt(strSignY,16);
                    int signZ=Integer.parseInt(strSignZ,16);
                    if(signX==1){
                        X*=-1;
                    }
                    if(signY==1){
                        Y*=-1;
                    }
                    if(signZ==1){
                        Z*=-1;
                    }
                    int heartRate=(int) Math.sqrt(Math.pow(X,2)+Math.pow(Y,2)+Math.pow(Z,2));
                    //int time = Integer.parseInt(strTime);
                    int time=i;
                    i++;
                    HealthDetails details = new HealthDetails(heartRate, time);
                    listHealthDetails.add(details);

                }

                    if(listHealthDetails.get(listHealthDetails.size()-1).getHeartRate()>HealthDetails.alertRate){
                        notification();
                    }
                    updateDataIncome();
                }
                else{
                    lineChart.clear();
                    lineChart.invalidate();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void notification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("n", "n", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getView().getContext().getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getView().getContext(),"n")
                .setContentText("Data received !!!")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(true)
                .setContentText("ALERT !!!");

        NotificationManagerCompat managerCompat=NotificationManagerCompat.from(getView().getContext());
        managerCompat.notify(999,builder.build());


    }
}
