package com.esigelec.huboc.geolocation;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx. annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.esigelec.huboc.R;
import com.esigelec.huboc.automation.AutomationDetails;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

public class GeolocationFragment extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    GoogleMap map;
    Marker marker;
    DatabaseReference db;
    GeolocationDetails location;
    TextView longitudeGeolocation;
    TextView latitudeGeolocation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.geolocation_fragment_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        latitudeGeolocation = view.findViewById(R.id.latitudeGeolocation);
        longitudeGeolocation = view.findViewById(R.id.longitudeGeolocation);

        db = FirebaseDatabase.getInstance("https://hubocbase-default-rtdb.europe-west1.firebasedatabase.app/").getReference().child("geolocation");
        location = new GeolocationDetails(0, 0);
        EventChangeListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        map = googleMap;
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latlng = new LatLng(latitude, longitude);
        marker=map.addMarker(new MarkerOptions().position(latlng).title(getCompleteAddress(latitude, longitude)));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 14F));
    }

    private  void updateMarker(GeolocationDetails location){
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latlng = new LatLng(latitude, longitude);
        marker.setPosition(latlng);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 14F));
        latitudeGeolocation.setText(latitude+" °");
        longitudeGeolocation.setText(longitude+" °");
    }

    private String getCompleteAddress(double latitude, double longitude) {
        String address = "";

        Geocoder geocoder = new Geocoder(getView().getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses != null) {
                Address returnAddress = addresses.get(0);
                StringBuilder stringBuilderReturnAddress = new StringBuilder("");

                for (int i = 0; i <= returnAddress.getMaxAddressLineIndex(); i++) {
                    stringBuilderReturnAddress.append(returnAddress.getAddressLine(i)).append("\n");
                }
                address = stringBuilderReturnAddress.toString();
            } else {
                Toast.makeText(getContext(), "Address not found", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }
        return address;
    }

    private void EventChangeListener() {
        Query lastQuery = db.orderByKey().limitToLast(1);
        lastQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String strCoordinates="";
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    strCoordinates = dataSnapshot.child("data").getValue().toString();

                }
                String strLatitudeInt=strCoordinates.substring(0,2);
                String strLatitudeDec1=strCoordinates.substring(2,4);
                String strLatitudeDec2=strCoordinates.substring(4,6);
                String strLatitudeDec3=strCoordinates.substring(6,8);
                String strLongitudeInt=strCoordinates.substring(8,10);
                String strLongitudeDec1=strCoordinates.substring(10,12);
                String strLongitudeDec2=strCoordinates.substring(12,14);
                String strLongitudeDec3=strCoordinates.substring(14,16);
                String strLatitudeSign=strCoordinates.substring(16,18);
                String strLongitudeSign=strCoordinates.substring(18,20);
                int iLatitudeInt = Integer.parseInt(strLatitudeInt,16);
                int iLatitudeDec = Integer.parseInt(strLatitudeDec1,16)*10000+Integer.parseInt(strLatitudeDec2,16)*100+Integer.parseInt(strLatitudeDec3,16);
                int iLongitudeInt = Integer.parseInt(strLongitudeInt,16);
                int iLongitudeDec = Integer.parseInt(strLongitudeDec1,16)*10000+Integer.parseInt(strLongitudeDec2,16)*100+Integer.parseInt(strLongitudeDec3,16);
                double latitude = (double)(iLatitudeInt+ (double)iLatitudeDec / 1000000);
                double longitude = (double)(iLongitudeInt+ (double)iLongitudeDec / 1000000);
                int signLat=Integer.parseInt(strLatitudeSign);
                int signLng=Integer.parseInt(strLongitudeSign);
                if(signLat==1){
                    latitude*=-1;
                }
                if(signLng==1){
                    longitude*=-1;
                }
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                updateMarker(location);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}